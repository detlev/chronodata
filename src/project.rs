use chrono::NaiveDate;
use serde::{Deserialize, Serialize};

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub enum ProjectCategory {
    Absence,
    Bench,
    Customer,
    Strategic,
    Other,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Project {
    pub id: u64,
    pub name: String,
    pub key: String,
    pub started: NaiveDate,
    pub finished: Option<NaiveDate>,
    pub is_rd: bool,
    pub is_billable: bool,
    pub is_absence: bool,
    #[serde(flatten)]
    other: serde_json::Value,
}

impl Project {
    pub fn category(&self) -> ProjectCategory {
        use ProjectCategory::*;
        if self.is_absence {
            return Absence;
        }

        match (self.is_billable, self.is_rd) {
            (true, false) => Customer,
            (true, true) => Strategic,
            (false, true) => Bench,
            _ => Other,
        }
    }
}
