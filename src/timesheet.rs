use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct User {
    pub full_name: String,
    pub login: String,
}
#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct TimesheetProject {
    pub id: u64,
    pub name: String,
    pub key: String,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Activity {
    pub id: u64,
    pub key: String,
    pub name: String,
    #[serde(flatten)]
    category: serde_json::Value,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct TimesheetItem {
    pub id: u64,
    pub project: TimesheetProject,
    pub duration: String,
    pub description: String,
    pub activity: Activity,
    #[serde(flatten)]
    other: serde_json::Value,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Timesheet {
    pub date: String,
    pub id: u64,
    pub user: User,
    pub total_time: u64,
    pub items: Vec<TimesheetItem>,
    #[serde(flatten)]
    other: serde_json::Value,
}
