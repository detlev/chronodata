mod paginator;
pub mod person;
pub mod project;
pub mod timesheet;
pub mod utils;

use chrono::NaiveDate;
use futures::prelude::*;
use log::debug;
use reqwest::{header, redirect::Policy, Client};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::convert::TryInto;
use tokio::sync::RwLock;
use url::Url;

use paginator::{PaginationError, Paginator};
use person::Person;
use project::Project;
use thiserror::Error;
use timesheet::Timesheet;

#[derive(Error, Debug)]
pub enum ChronophageError {
    #[error("Could not parse url")]
    ParseUrlError(#[from] url::ParseError),
    #[error("Invalid token format")]
    InvalidToken(#[from] header::InvalidHeaderValue),
    #[error("Failed to build reqwest client")]
    ReqwestError(#[from] reqwest::Error),
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Domain {
    pub id: u32,
    pub name: String,
    pub department: u32,
}

#[derive(Debug)]
pub struct Chronophage {
    client: Client,
    base: Url,
    domains: RwLock<HashMap<u32, Domain>>,
}

impl Chronophage {
    pub fn new(url: &str, token: Option<String>) -> Result<Chronophage, ChronophageError> {
        let host: Url = url.parse()?;
        let base = host.join("rest-api/")?;
        let mut headers = header::HeaderMap::new();

        if let Some(t) = token {
            headers.insert(
                reqwest::header::AUTHORIZATION,
                format!("Token {}", t).try_into()?,
            );
        }

        // Force redirect policy none as that will drop sensitive headers; in
        // particular tokens
        let client = Client::builder()
            .redirect(Policy::none())
            .default_headers(headers)
            .build()?;

        Ok(Chronophage {
            client,
            base,
            domains: RwLock::new(HashMap::new()),
        })
    }

    async fn refresh_domains(&self) -> Result<(), PaginationError> {
        debug!("Refreshing domains");
        let mut domains = self.domains.write().await;
        let mut new: Paginator<Domain> =
            Paginator::new(self.client.clone(), &self.base, "domains/");

        while let Some(d) = new.try_next().await? {
            domains.insert(d.id, d);
        }

        Ok(())
    }

    pub async fn domain(&self, id: u32) -> Option<String> {
        {
            let domains = self.domains.read().await;
            if let Some(d) = domains.get(&id) {
                return Some(d.name.clone());
            }
        }
        let _ = self.refresh_domains().await.unwrap();
        let domains = self.domains.read().await;
        domains.get(&id).map(|d| d.name.clone())
    }

    pub async fn domains(&self) -> Vec<Domain> {
        {
            let domains = self.domains.read().await;
            if domains.len() > 0 {
                return domains.values().map(|d| d.clone()).collect();
            }
        }
        let _ = self.refresh_domains().await.unwrap();
        let domains = self.domains.read().await;
        domains.values().map(|d| d.clone()).collect()
    }

    pub fn timesheets(&self, id: u64, from: &NaiveDate, to: &NaiveDate) -> Paginator<Timesheet> {
        let path = format!(
            "timesheets/?project_id={}&date__gte={}&date__lte={}",
            id, from, to
        );
        Paginator::new(self.client.clone(), &self.base, &path)
    }

    pub fn projects(&self) -> Paginator<Project> {
        Paginator::new(self.client.clone(), &self.base, "projects/")
    }

    pub fn people(&self) -> Paginator<Person> {
        Paginator::new(self.client.clone(), &self.base, "people/")
    }
}
