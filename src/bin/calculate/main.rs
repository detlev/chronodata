use anyhow::Error;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::File;
use structopt::StructOpt;

use chronodata::person::Person;
use chronodata::project::{Project, ProjectCategory};
use chronodata::timesheet::Timesheet;
use chronodata::Domain;

#[derive(StructOpt)]
struct Opt {
    input: String,
}

fn display_hours(duration: u64, total: u64) -> String {
    format!(
        "{}:{} ({}%)",
        duration / 3600,
        (duration % 3600) / 60,
        duration * 100 / total
    )
}

#[derive(Clone, Serialize, Deserialize, Debug)]
struct Data {
    people: Vec<Person>,
    projects: Vec<Project>,
    timesheets: Vec<Timesheet>,
    domains: Vec<Domain>,
}

impl Data {
    fn get_project(&self, id: u64) -> Option<Project> {
        self.projects.iter().find(|p| p.id == id).cloned()
    }

    fn get_domain(&self, id: u32) -> Option<Domain> {
        self.domains.iter().find(|p| p.id == id).cloned()
    }

    fn get_person(&self, username: &str) -> Option<Person> {
        self.people.iter().find(|p| p.username == username).cloned()
    }
}

#[derive(Clone, Debug, Default)]
struct CategoryData {
    total: u64,
    categories: HashMap<ProjectCategory, u64>,
}

#[derive(Clone, Debug, Default)]
struct DomainData {
    domain: CategoryData,
    users: HashMap<String, CategoryData>,
}

lazy_static! {
    static ref OVERRIDE: HashMap<&'static str, &'static str> = {
        let mut m = HashMap::new();
        m.insert("sjoerd", "bsl");
        m.insert("padovan", "bsl");
        m.insert("daniels", "bsl");
        m.insert("kov", "bsl");
        m.insert("tester", "bsl");

        m.insert("leandrohrb", "internships");
        m.insert("italo", "internships");
        m
    };
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let env = env_logger::Env::default()
        .filter_or("CHRONO_LOG", "chronophage=info")
        .write_style("CHRONO_WRITE_STYLE");
    env_logger::init_from_env(env);

    let opts = Opt::from_args();

    let dump = File::open(&opts.input)?;
    let data: Data = serde_json::from_reader(&dump)?;

    /*
    for p in data.projects.iter() {
        println!("{} - {} - {:?}", p.key, p.name, p.category());
    }
    */

    let mut split: HashMap<String, DomainData> = HashMap::new();
    for t in data.timesheets.iter() {
        let person = data
            .get_person(&t.user.login)
            .expect(&format!("User {} not found", t.user.login));

        let domain = match OVERRIDE.get(person.username.as_str()) {
            Some(d) => d.to_string(),
            None => {
                let d = person.domain.expect("Person without domain in data!?!?");
                data.get_domain(d).expect("Domain not found").name
            }
        };

        let d = split.entry(domain).or_default();
        let u = d.users.entry(person.username).or_default();
        for i in t.items.iter() {
            let project = data.get_project(i.project.id).unwrap();
            let category = project.category();

            if category == ProjectCategory::Absence {
                continue;
            }

            let duration: u64 = i.duration.parse().expect("Failed to parse duration");

            let current = u.categories.entry(category.clone()).or_default();
            *current += duration;
            u.total += duration;

            let current = d.domain.categories.entry(category).or_default();
            *current += duration;
            d.domain.total += duration;
        }
    }

    for (domain, d) in split.iter() {
        println!("== {} ==", domain);
        for (user, userdata) in d.users.iter() {
            print!("{}: ", user);
            for (cat, time) in userdata.categories.iter() {
                print!("{:?}: {}  ", cat, display_hours(*time, userdata.total));
            }
            println!("");
        }
        println!("");
        print!("{} total: ", domain);
        for (cat, time) in d.domain.categories.iter() {
            print!("{:?}: {}  ", cat, display_hours(*time, d.domain.total));
        }
        println!("");
    }

    Ok(())
}
