use anyhow::Error;
use chrono::NaiveDate;
use chronodata::Chronophage;
use futures::prelude::*;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::OpenOptions;
use structopt::StructOpt;

use chronodata::person::Person;
use chronodata::project::Project;
use chronodata::timesheet::Timesheet;
use chronodata::Domain;

#[derive(StructOpt)]
struct Opt {
    #[structopt(short, long, default_value = "https://chronophage.collabora.com")]
    url: String,
    #[structopt(short, long)]
    token: Option<String>,
    #[structopt(short, long, default_value = "dump.json")]
    output: String,
    from: NaiveDate,
    to: NaiveDate,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
struct Data {
    people: Vec<Person>,
    projects: Vec<Project>,
    timesheets: Vec<Timesheet>,
    domains: Vec<Domain>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let env = env_logger::Env::default()
        .filter_or("CHRONO_LOG", "chronophage=info")
        .write_style("CHRONO_WRITE_STYLE");
    env_logger::init_from_env(env);

    let opts = Opt::from_args();
    let c = Chronophage::new(&opts.url, opts.token)?;

    let mut people = HashMap::new();

    let mut p = c.people();
    while let Some(p) = p.try_next().await? {
        if p.domain.is_some() {
            people.insert(p.username.clone(), p);
        }
    }

    let mut p = c.projects();
    let mut projects = HashMap::new();
    while let Some(p) = p.try_next().await? {
        let finished = match p.finished {
            Some(date) if date < opts.from => true,
            _ => false,
        };

        if !finished {
            println!(
                "{:#?} {} {} {}",
                p.name, p.is_rd, p.is_billable, p.is_absence
            );
            projects.insert(p.id, p);
        }
    }

    let mut timesheets = HashMap::new();

    for i in projects.keys() {
        let mut t = c.timesheets(*i, &opts.from, &opts.to);
        while let Some(t) = t.try_next().await? {
            if people.contains_key(&t.user.login) {
                timesheets.insert(t.id, t);
            }
        }
    }

    let data = Data {
        people: people.values().map(Person::clone).collect(),
        projects: projects.values().map(Project::clone).collect(),
        timesheets: timesheets.values().map(Timesheet::clone).collect(),
        domains: c.domains().await,
    };

    let dump = OpenOptions::new()
        .write(true)
        .create_new(true)
        .open(&opts.output)?;

    serde_json::to_writer(&dump, &data)?;

    Ok(())
}
